
echo "Register"

read -p "Enter username: " username

if grep -q "^$username," users.txt; then
	echo "Username already exists"
	echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> log.txt
	exit 1
fi

while true; do
	read -s -p  "Enter password: " password
echo
	if [[ "$password" == "$username" ]]; then
					echo "Password must not be the same as username"
		continue
	fi
	if [[ ${#password} -lt 8 ]]; then 
					echo "Password must be at least 8 characters long"
		continue
	fi
	if [[ ! "$password" =~ [a-z] ]]; then
					echo "Password must contain at least one lowercase letter"
		continue
	fi
	if [[ ! "$password" =~ [A-Z] ]]; then
					echo "Password must contain at least one uppercase letter"
		continue
	fi
	if [[ ! "$password" =~ [0-9] ]]; then
					echo "Password must contain at least one number"
		continue
	fi
	if [[ "$password" =~ [Cc][Hh][Ii][Cc][Kk][Ee][Nn] ]]; then
					echo "Password must not contain the word 'chicken'"
		continue
	fi
	if [[ "$password" =~ [Ee][Rr][Nn][Ii][Ee] ]]; then
					echo "Password must not contain the word 'ernie'"
		continue
	fi
	break
done
echo "$username, $password" >> users.txt
echo "User registered successfully"
echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully" >> log.txt;
