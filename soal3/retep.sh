echo "Login"

read -p "Enter Username:" username
read -s -p "Enter password:" password
echo

if grep -q "^$username, $password" users.txt; then
	echo "Login successful"
	echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
else
	echo "Invalid username or password"
	echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
	exit 1
fi
