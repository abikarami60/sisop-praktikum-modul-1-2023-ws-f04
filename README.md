# Praktikum Sistem Operasi Modul 1
# Kelompok F04
- Fathan Abi Karami 5025211156
- Heru Dwi Kurniawan 5025211055
- Alya Putri Salma 5025211174





# Soal 1

Bocchi hendak melakukan University Admission Test di Jepang. Bocchi ingin masuk ke universitas yang bagus. Akan tetapi, dia masih bingung sehingga ia memerlukan beberapa strategi untuk melakukan hal tersebut. Untung saja Bocchi menemukan file .csv yang berisi ranking universitas dunia untuk melakukan penataan strategi  : 

```bash
echo "-----Top 5 University in Japan-----"
awk -F ',' '/JP/ {print $1,$2,$9; count++} count==5{exit} ' '2023 QS World University Rankings.csv'

echo "-----University with lowest FSR-----"
awk -F ',' '/JP/ {print $9,$2}' '2023 QS World University Rankings.csv' | sort  -g | head -1 
echo "-----GER tertinggi-----"
awk -F ',' '/JP/ {print $19,$2}' '2023 QS World University Rankings.csv' | sort  -g -r | head -10 
echo "-----Universitas Keren---"
awk -F ',' '/Keren/ {print $1,$2}' '2023 QS World University Rankings.csv'  

echo "done"
```

## point 1

Bocchi ingin masuk ke universitas yang bagus di Jepang. Oleh karena itu, Bocchi perlu melakukan survey terlebih dahulu. Tampilkan 5 Universitas dengan ranking tertinggi di Jepang.

```bash

echo "-----Top 5 University in Japan-----"
awk -F ',' '/JP/ {print $1,$2,$9; count++} count==5{exit} ' '2023 QS World University Rankings.csv'

```
Penjelasan: Menggunakan awk -F ',' untuk membagi line menjadi beberapa field. lalu /JP/ untuk mendapatkan universitaas dari jepang. menggunakan count untuk keep track. Karena sudah urut ranking maka tidak perlu di sort.

## point 2

Karena Bocchi kurang percaya diri dengan kemampuannya, coba cari Faculty Student Score(fsr score) yang paling rendah diantara 5 Universitas di Jepang. 
```bash
echo "-----University with lowest FSR-----"
awk -F ',' '/JP/ {print $9, $2;}' '2023 QS World University Rankings.csv'| sort  -g | head -5
```
penjelasan: sama seperti point 1, bagi line menjadi beberapa field. lalu cari universitas asal jepang dengan /JP/. lalu print FSR score dan nama universitasnya. setelah didapat lalu lakukan pipe. lalu lakukan sort. pipe lagi, lalu gunakan head -5 untuk ambil 5 baris paling atas.

## point 3
Karena Bocchi takut salah membaca ketika memasukkan nama universitas, cari 10 Universitas di Jepang dengan Employment Outcome Rank(ger rank) paling tinggi.
```bash
echo "-----GER tertinggi-----"
awk -F ',' '/JP/ {print $19,$2}' '2023 QS World University Rankings.csv' | sort  -g -r | head -10 
```

penjelasan: sama seperti point 1 , bagi line menjadi beberapa field dan cari universitas asal jepang. lalu print GER score dan nama universitas. lalu lakukan pipe sort reverse untuk mendapatkan urutan descending. lalu pipe lagi head -10 untuk mendapat top 10

## point 4

Bocchi ngefans berat dengan universitas paling keren di dunia. Bantu bocchi mencari universitas tersebut dengan kata kunci keren.

```bash
echo "-----Universitas Keren---"
awk -F ',' '/Keren/ {print $1,$2}' '2023 QS World University Rankings.csv'  

echo "done"
```
penjelasan: sama seperti point 1, bagi line menjedi beberapa field. gunakan /Keren/ untuk mencari universitas paling keren. lalu print :)

output terminal:
![](./img/ss_terminal_soal1.png)

## Kendala dan Error
Beberapa kendala dan error saat mengerjakan soal 1 adalah: fungsi sort yang bermasalah saat menggunakan -n (misalnya pada soal 2, universitas terendah adalah sophia university, karena 20 < 2.5), perbaikan dengan menngunakan -g

# Soal 2
Kobeni ingin pergi ke negara terbaik di dunia bernama Indonesia. Akan tetapi karena uang Kobeni habis untuk beli headphone ATH-R70x, Kobeni tidak dapat melakukan hal tersebut. 

# Poin 1
Untuk melakukan coping, Kobeni mencoba menghibur diri sendiri dengan mendownload gambar tentang Indonesia. Coba buat script untuk mendownload gambar sebanyak X kali dengan X sebagai jam sekarang (ex: pukul 16:09 maka X nya adalah 16 dst. Apabila pukul 00:00 cukup download 1 gambar saja). Gambarnya didownload setiap 10 jam sekali mulai dari saat script dijalankan. Adapun ketentuan file dan folder yang akan dibuat adalah sebagai berikut:
- File yang didownload memilki format nama “perjalanan_NOMOR.FILE” Untuk NOMOR.FILE, adalah urutan file yang download (perjalanan_1, perjalanan_2, dst)
- File batch yang didownload akan dimasukkan ke dalam folder dengan format nama “kumpulan_NOMOR.FOLDER” dengan NOMOR.FOLDER adalah urutan folder saat dibuat (kumpulan_1, kumpulan_2, dst) 
```
#Membuat folder new dengan nama kumpulan_(num_folder)

    last_folder=$(ls -1d "$DIR"kumpulan_* | sort -Vr | head -n 1) #berfungsi untuk mengecek angka folder terakhir
    
    if [ -z "$last_folder" ]; then #apakah suatu variabel kosong atau tidak
        #jika num_folder kosong maka num folder di set ke value 1
		num_folder=1 
    else 
		#untuk mengambil angka dari folder yag terakhir
        num_folder=$(basename "$last_folder" | sed 's/kumpulan_//') 
        num_folder=$((num_folder + 1)) #untuk penambahan folder 
	#selesai program
    fi 
	#membuat folder baru dengan nama kumpulan _ 1
    new_folder="${DIR}kumpulan_${num_folder}" 
    mkdir "$new_folder" 
     
    # Mendownload gambar-gambar dari unsplash sejumlah N, dimana N adalah waktu saat ini dalam jam
	#mengecek apakah jam tersebut sama dengan 0 
    if [ "$HOUR" -eq 0 ]; then 
        NUM_IMAGES=1 # jika 0 maka donlod 1
    else 
	# jika tidak maka download sesuai perintah di jam tersebut
        NUM_IMAGES=$HOUR 
	# program berhenti
    fi 
	#perintah untuk download gambar 
    for (( i=1; i<=$NUM_IMAGES; i++ )); do 
	#komen untuk download
        wget -O "$new_folder/perjalanan_$i.jpg" "https://source.unsplash.com/random/?indonesia" 
    done
```

# Poin 2
Karena Kobeni uangnya habis untuk reparasi mobil, ia harus berhemat tempat penyimpanan di komputernya. Kobeni harus melakukan zip setiap 1 hari dengan nama zip “devil_NOMOR ZIP” dengan NOMOR.ZIP adalah urutan folder saat dibuat (devil_1, devil_2, dst). Yang di ZIP hanyalah folder kumpulan dari soal di atas.
```
   #Fungsi untuk Zip folder di menit terakhir di hari itu
    if [ "$HOUR" -eq 23 ] && [ "$MINUTE" -eq 59 ]; then
	# Fungsi mengambil angka terakhir dari zip devil
    	lasted_num=$(ls -1d "$DIR"devil_* | sort -Vr | head -n 1) 
	if [ -z "$lasted_num" ]
	then
	# Jika belum ada zip devil maka nomor zipnya dimulai dari angka 1
    		zip_count=1 
	else
	# memfilter nama zip untuk mendapatkan angkanya
    		zip_count=$(basename "$lasted_num" | sed 's/devil_//' | sed 's/\.zip//') 
			 # angka yang didapatkan ditambah 1
    		zip_count=$((zip_count + 1))
	fi
	# untuk membuat format direktori baru
	zip_dir="${DIR}devil_${zip_count}" 
	 # untuk membuat direktori baru
	mkdir "$zip_dir"
	#memindahkan folder kumpulan ke folder zip
	find "$DIR" -maxdepth 1 -type d -name "kumpulan*" -exec mv {} "$zip_dir" \; 
	
	# untuk membuat format penamaan zip
	zip_archive="${zip_dir}.zip" 
	# zip folder devil 
	zip -r "$zip_archive" "devil_${zip_count}" 
	 # untuk remove/hapus folder zip
	rm -rf "$zip_dir"
	exit 0
	# program selesai
    fi 
```
# Poin 3 ( Crontab )
```
#Fungsi inisialisasi cronjob
    #0 */10 * * * /bin/bash /home/herukurniawan/Documents/sisop_soal_2/kobeni_liburan.sh  
    #59 23 * * * /bin/bash /home/herukurniawan/Documents/sisop_soal_2/kobeni_liburan.sh
```
# screenshots Crontab

![Screenshots crontab](img/ss_Crontab_soal2.jpeg)


# Poin 4 ( Fungsi untuk mendapatkan waktu Wib )

```
#Untuk mendapatkan jam saat ini
    HOUR=$(TZ=Asia/Jakarta date +"%H") 
    HOUR=$(expr $HOUR + 0 ) 
#Untuk mendapatkan menit saat ini
    MINUTE=$(TZ=Asia/Jakarta date +"%M")
```
# Kendala dan Error
Untuk kendala dan error saat praktikum pada soal 2 adalah masih terdapat error pada saat implementasi crontabnya, dan untuk catatan saat demo untuk merevisi dan memperbaiki fungsi crontabnya pada soal 2


# Soal 3

Peter Griffin hendak membuat suatu sistem register pada script louis.sh dari setiap user yang berhasil didaftarkan di dalam file /users/users.txt. 

Setiap percobaan login dan register akan tercatat pada log.txt dengan format : YY/MM/DD hh:mm:ss MESSAGE. Message pada log akan berbeda tergantung aksi yang dilakukan user. 

Untuk memastikan password pada register dan login aman, maka ketika proses input passwordnya harus memiliki ketentuan berikut:


# louis.sh
- Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
```bash
echo "Register"

read -p "Enter username: " username

if grep -q "^$username," users.txt; then
	echo "Username already exists"
	echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: ERROR User already exists" >> log.txt
	exit 1
fi
```
Penjelasan: Implementasi untuk memastikan bahwa usernme yang dimasukkan belum pernah ada sebelumnya.

`grep -q` untuk mencari username dan password tanpa menampilkan hasilnya

- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username 
- Tidak boleh menggunakan kata chicken atau ernie
```bash
while true; do
	read -s -p  "Enter password: " password
echo
	if [[ "$password" == "$username" ]]; then
					echo "Password must not be the same as username"
		continue
	fi
	if [[ ${#password} -lt 8 ]]; then 
					echo "Password must be at least 8 characters long"
		continue
	fi
	if [[ ! "$password" =~ [a-z] ]]; then
					echo "Password must contain at least one lowercase letter"
		continue
	fi
	if [[ ! "$password" =~ [A-Z] ]]; then
					echo "Password must contain at least one uppercase letter"
		continue
	fi
	if [[ ! "$password" =~ [0-9] ]]; then
					echo "Password must contain at least one number"
		continue
	fi
	if [[ "$password" =~ [Cc][Hh][Ii][Cc][Kk][Ee][Nn] ]]; then
					echo "Password must not contain the word 'chicken'"
		continue
	fi
	if [[ "$password" =~ [Ee][Rr][Nn][Ii][Ee] ]]; then
					echo "Password must not contain the word 'ernie'"
		continue
	fi
	break
done
```
Penjelasan: Implementasi agar password yang dimasukkan sesuai dengan ketentuan-ketentuan yang telah ditentukan dengan menggunakan perulangan `if else` dan `while` condition.

`-s` berfungsi agar password yang diinputkan tidak terlihat oleh user.

`-lt8` untuk memastikan jika password yang dimasukkan user minimal 8 karakter

Output terminal:

![](./img/ss_terminal_louis.png)

- Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
```bash
echo "$username, $password" >> users.txt
echo "User registered successfully"
echo "$(date +"%y/%m/%d %H:%M:%S") REGISTER: INFO User $username registered successfully" >> log.txt;
```
Penjelasan: Implementasi ketika register berhasil dilakukan.
log.txt:

![](./img/ss_log_louis.png)

# retep.sh
- Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in
```bash
echo "Login"

read -p "Enter Username:" username
read -s -p "Enter password:" password
echo

if grep -q "^$username, $password" users.txt; then
	echo "Login successful"
	echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: INFO User $username logged in" >> log.txt
```
Penjelasan: Implementasi ketika login berhasil dilakukan.
grep -q untuk mencari username dan password tanpa menampilkan hasilnya

- Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
```bash
else
	echo "Invalid username or password"
	echo "$(date +'%y/%m/%d %H:%M:%S') LOGIN: ERROR Failed login attempt on user $username" >> log.txt
	exit 1
fi
```
Panjelasan: Implementasi ketika login gagal dilakukan.

log.txt:

![](./img/ss_log_retep.png)

## Kendala dan Error

Kendala dan error-nya yaitu masih salah dalam menggunakan fungsi untuk membuat ketentuan-ketentuan dari sistem login.



# Soal 4

Johan Liebert adalah orang yang sangat kalkulatif. Oleh karena itu ia mengharuskan dirinya untuk mencatat log system komputernya. File syslog tersebut harus memiliki ketentuan : 
- Backup file log system dengan format jam:menit tanggal:bulan:tahun (dalam format .txt).

- Isi file harus dienkripsi dengan string manipulation yang disesuaikan dengan jam dilakukannya backup seperti berikut:
    - Menggunakan sistem cipher dengan contoh seperti berikut. Huruf b adalah alfabet kedua, sedangkan saat ini waktu menunjukkan pukul 12, sehingga huruf b diganti dengan huruf alfabet yang memiliki urutan ke 12+2 = 14
    - Hasilnya huruf b menjadi huruf n karena huruf n adalah huruf ke empat belas, dan seterusnya.
    - Setelah huruf z akan kembali ke huruf a
- Buat juga script untuk dekripsinya.
- Backup file syslog setiap 2 jam untuk dikumpulkan 😀.

## log_encrypt
```bash
echo "Encrypting..."

input="/var/log/syslog"
output=$(date +"%H:%M %d:%m:%y")

key=$(date "+%H")

upperCase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
lowerCase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)

upper="${upperCase[$key]}-ZA-${upperCase[$key-1]%26}"
lower="${lowerCase[$key]}-za-${lowerCase[$key-1]%26}"


echo -n "$(cat "$input" | tr 'a-z' $lower | tr 'A-Z' $upper)" > "$output.txt"


echo "done encrypting"



# cron job
# 0 */2 * * * /home/fathan/praktikum_modul_1/soal4/log_encrypt.sh
```
Penjelasan: input berupa syslog. output ke file dengan format "HH:MM dd:mm:yy.txt". Ambil key dari jam dimana script dijalankan. buat cipher untuk upper case dan lower case sesuai key. tampilkan input lalu transform 'a-z' ke cipher lower lalu transform lagi 'A-Z' ke cipher upper. terakhir outputkan ke file output

input file:
![](./img/ss_file_syslog.png)

output terminal:
![](./img/ss_terminal_logencrypt.png)

output file:
![](./img/ss_encrypted_syslog.png)



contoh output file:


## log_decrypt
```bash
echo "Decrypting..."

# contoh input file
input="11:11 10:03:23.txt"

output="decrypted.txt"

encryptKey=${input:0:2}
let key=26-$encryptKey


upperCase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
lowerCase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)

upper="${upperCase[$key]}-ZA-${upperCase[$key-1]%26}"
lower="${lowerCase[$key]}-za-${lowerCase[$key-1]%26}"


echo -n "$(cat "$input" | tr 'a-z' $lower | tr 'A-Z' $upper)" > "$output"


echo "done decrypting"

```
Penjelasan: input berupa syslog yang telah dienkripsi. output ke file dengan format "HH:MM dd:mm:yy.txt". Ambil encryptedKey dari jam yang ada di nama file input. lakukan operasi key = 26 - encryptedKey untuk membalikkan enkripsi. buat cipher untuk upper case dan lower case sesuai key tampilkan input lalu transform 'a-z' ke cipher lower lalu transform lagi 'A-Z' ke cipher upper. terakhir outputkan ke file output



input file:
![](./img/ss_encrypted_syslog.png)

output terminal:
![](./img/ss_terminal_logdecrypt.png)

output file:
![](./img/ss_decrypted_syslog.png)

## kendala dan error
kendala dan error saat mengerjakan soal 4 adalah terdapat error di terminal: 
```txt
log_encrypt: line 15: warning: command substitution: ignore null byte in input
```
akan tetapi, script tetap berjalan seperti yang dimaksud


# REVISI

## soal 1
pada point 2 mengganti yang awalnya menampilkan 1 universitas jepang dengan FSR terendah menjadi 5 universitas di jepang dengan FSR terendah

## soal 2
Memperbaiki dan menambah lagi fungsi crontabnya

## soal 3
Mengganti penggunaan fungsi pada ketentuan-ketentuan dalam sistem login

## soal 4
Mengganti algoritma yang awalnya menggunakan metode operasi aritmatika ascii value menjadi metode cipher



