echo "-----Top 5 University in Japan-----"
awk -F ',' '/JP/ {print $1,$2,$9; count++} count==5{exit} ' '2023 QS World University Rankings.csv'

echo "-----University with lowest FSR-----"
awk -F ',' '/JP/ {print $9, $2;}' '2023 QS World University Rankings.csv'| sort  -g | head -5
echo "-----GER tertinggi-----"
awk -F ',' '/JP/ {print $19,$2}' '2023 QS World University Rankings.csv' | sort  -g -r | head -10 
echo "-----Universitas Keren---"
awk -F ',' '/Keren/ {print $1,$2}' '2023 QS World University Rankings.csv'  

echo "done"
