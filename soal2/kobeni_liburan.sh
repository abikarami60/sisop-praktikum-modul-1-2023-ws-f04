#!/bin/bash #file kobeni_liburan akan dieksekusi dengan shell bash 
    
    #Crontab
    #0 */10 * * * /bin/bash /home/herukurniawan/Documents/sisop_soal_2/kobeni_liburan.sh  
    #59 23 * * * /bin/bash /home/herukurniawan/Documents/sisop_soal_2/kobeni_liburan.sh

    #direktori bahwa file kobeni_liburan.sh ini disimpan di folder documents
    DIR="/home/herukurniawan/Documents/sisop_soal_2/" 
     
    # Fungsi untuk mendapatkan jam saat ini
    HOUR=$(TZ=Asia/Jakarta date +"%H")
    #mengkonvert jam misal 09.00 >> 9
    HOUR=$(expr $HOUR + 0 ) 
    #Fungsi untuk menit saat ini
    MINUTE=$(TZ=Asia/Jakarta date +"%M")
    
   #Fungsi untuk Zip folder di menit terakhir di hari itu
    if [ "$HOUR" -eq 23 ] && [ "$MINUTE" -eq 59 ]; then
    # Fungsi mengambil angka terakhir dari zip devil
    	lasted_num=$(ls -1d "$DIR"devil_* | sort -Vr | head -n 1) 
	if [ -z "$lasted_num" ]
	then
    # Jika belum ada zip devil maka nomor zipnya dimulai dari angka 1
    		zip_count=1 
	else
    # memfilter nama zip untuk mendapatkan angkanya
    		zip_count=$(basename "$lasted_num" | sed 's/devil_//' | sed 's/\.zip//') 
            # angka yang didapatkan ditambah 1
    		zip_count=$((zip_count + 1)) 
	fi
    # untuk membuat format direktori baru
	zip_dir="${DIR}devil_${zip_count}" 
    # untuk membuat direktori baru
	mkdir "$zip_dir" 
    #memindahkan folder kumpulan ke folder zip
	find "$DIR" -maxdepth 1 -type d -name "kumpulan*" -exec mv {} "$zip_dir" \; 

    # untuk membuat format penamaan zip
	zip_archive="${zip_dir}.zip" 
    # zip folder devil 
	zip -r "$zip_archive" "devil_${zip_count}" 
    # untuk remove/hapus folder zip
	rm -rf "$zip_dir" 
	exit 0
    # program selesai
    fi 
    
    # Membuat folder baru dengan nama kumpulan_{num_folder}
    #berfungsi untuk mengecek angka folder terakhir
    last_folder=$(ls -1d "$DIR"kumpulan_* | sort -Vr | head -n 1) 
    #apakah suatu variabel kosong atau tidak
    if [ -z "$last_folder" ]; then
    # jika num_folder kosong maka num folder di set ke value 1
        num_folder=1 
    else 
    #untuk mengambil angka dari folder yag terakhir
        num_folder=$(basename "$last_folder" | sed 's/kumpulan_//') 
        #untuk penambahan folder
        num_folder=$((num_folder + 1))  
        #selesai program
    fi 
    #membuat folder baru dengan nama kumpulan _ 1
    new_folder="${DIR}kumpulan_${num_folder}" 
    mkdir "$new_folder" 
     
    # Mendownload gambar-gambar dari unsplash sejumlah x,x adalah waktu saat ini dalam jam tersebut
     #mengecek apakah jam tersebut sama dengan 0 
    if [ "$HOUR" -eq 0 ]; then
    # jika 0 maka donlod 1
        NUM_IMAGES=1 
    else 
    # jika tidak maka donlod sesuai perintah di jam tersebut
        NUM_IMAGES=$HOUR 
    fi # program berhenti
    #perintah untuk download gambar 
    for (( i=1; i<=$NUM_IMAGES; i++ )); do 
    #komen untuk download gambar
        wget -O "$new_folder/perjalanan_$i.jpg" "https://source.unsplash.com/random/?indonesia" 
    done
