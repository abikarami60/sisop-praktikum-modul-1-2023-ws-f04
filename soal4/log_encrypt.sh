echo "Encrypting..."

input="/var/log/syslog"
output=$(date +"%H:%M %d:%m:%y")

key=$(date "+%H")

upperCase=(A B C D E F G H I J K L M N O P Q R S T U V W X Y Z)
lowerCase=(a b c d e f g h i j k l m n o p q r s t u v w x y z)

upper="${upperCase[$key]}-ZA-${upperCase[$key-1]%26}"
lower="${lowerCase[$key]}-za-${lowerCase[$key-1]%26}"


echo -n "$(cat "$input" | tr 'a-z' $lower | tr 'A-Z' $upper)" > "$output.txt"


echo "done encrypting"



# cron job
# 0 */2 * * * /home/fathan/praktikum_modul_1/soal4/log_encrypt.sh